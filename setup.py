from setuptools import setup

setup(
	name = 'grijata',
	version = '0.0.1',
	description = 'A grid-style Jack audio connection manager',
	url = 'https://bitbucket.org/lorenzosu/grijata',
	author = 'Lorenzo Sutton, Antonio Malara',
	packages = ['grijata'],
	install_requires = [
		'pgi',
		'psutil',
	],
	entry_points = {
		'console_scripts': [
			'grijata = grijata.grijata:main'
		]
	}
)
