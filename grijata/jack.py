from collections import defaultdict

class Graph(object):
    def __init__(self):
        self.clients_out_dic = defaultdict(list)
        self.clients_in_dic  = defaultdict(list)
        self.out_ports = []
        self.in_ports  = []


    def __eq__(self, other):
        return (
            self.clients_out_dic == other.clients_out_dic and
            self.clients_in_dic  == other.clients_in_dic  and
            self.out_ports       == other.out_ports       and
            self.in_ports        == other.in_ports
        )


    def __ne__(self, other):
        return not self == other


class Port:
    """ Class to represent a jack port (not necessarily comprehensive)

        Reference:
        http://jackaudio.org/files/docs/html/types_8h.html#acbcada380e9dfdd5bff1296e7156f478
    """

    def __init__(self, full_port_name):
        """ Set parameters for a port """
        self.full_port_name = full_port_name
        self.is_input       = None
        self.is_output      = None
        self.is_physical    = None
        self.can_monitor    = None
        self.is_terminal    = None
        self.connections    = []
        (self.client_name,
         self.port_name)    = full_port_name.split(':', 1)


    def __eq__(self, other):
        return (
            self.full_port_name == other.full_port_name and
            self.is_input       == other.is_input       and
            self.is_output      == other.is_output      and
            self.is_physical    == other.is_physical    and
            self.can_monitor    == other.can_monitor    and
            self.is_terminal    == other.is_terminal    and
            self.connections    == other.connections 
        )

