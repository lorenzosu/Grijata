import jack
import subprocess
import threading
import time

class EventNames:
    graph_updated = "graph_updated"
    error         = "error"
    

def get_current_graph():
    """
    returns: either<jack.Graph: in case of success,
                    None:       in case of error>
    """

    output, _, ret_code = _run_command([
        "jack_lsp",  # list ports
        "-cp",       # port connections, port properties
    ])
    return _parse_lsp_output(output) if ret_code == 0 else None


def connect_ports(output_port, input_port):
    """
    output_port: jack.Port
    input_port:  jack.Port
    returns:     either<None:   in case of success,
                        String: in case of error>
    """

    _, error, returncode = _run_command([
        'jack_connect',
        output_port.full_port_name,
        input_port.full_port_name
    ])

    return None if returncode == 0 else error


def disconnect_ports(output_port, input_port):
    """
    output_port: jack.Port
    input_port:  jack.Port
    returns:     either<None:   in case of success,
                        String: in case of error>
    """

    _, error, returncode = _run_command([
        'jack_disconnect',
        output_port.full_port_name,
        input_port.full_port_name
    ])

    return None if returncode == 0 else error


# - #


def listen_to_graph_updates(deliver_event):
    def jack_updater_thread():
        print "Starting jack updates..."

        while True:
            print "Poll..."

            graph = get_current_graph()
            if graph is None:
                # TODO In theory jack could also be closed *after* so this should really
                # be something like 'Exit' or 'Retry'

                deliver_event(
                    EventNames.error, 
                    "jack_lsp returned an error.\nIs jack runing??",
                )
            else:
                deliver_event(
                    EventNames.graph_updated,
                    graph
                )

            time.sleep(1)

    t = threading.Thread(
        name   = "Jack Updater Thread",
        target = jack_updater_thread,
    )

    t.daemon = True
    t.start()

# - #


def _parse_lsp_output(lsp_output):
    """
    Parse the output of 'jack_lsp -cp' and populate a dictionary with a
    list of ports. The dictionary keys are all the jack clients. Each value
    is a list of jack ports of the jack.Port type.
    """

    output_lines = lsp_output.split("\n")
    try:
        output_lines.pop()
    except IndexError:
        # TODO why? What happened
        print "Empty output? Something went wrong..."
        return None
 
    graph = jack.Graph()

    current_port = None
    for line in output_lines:
        if not line[0].isspace():
            # This is the client:port name
            full_port_name = line
            current_port = jack.Port(full_port_name)
            continue
        if line.find('properties:') < 0:
            # this must be a connection
            conn = line.strip()
            current_port.connections.append(conn)
        else:
            # Finally these must be the properties
            current_port.is_input = line.find('input') >= 0
            current_port.is_output = line.find('output') >= 0
            current_port.is_physical = line.find('physical') >= 0
            current_port.is_terminal = line.find('terminal') >= 0

            # Properties come at the end so we finally append the port
            cli_name = current_port.client_name
            if current_port.is_output:
                graph.out_ports.append(current_port)
                graph.clients_out_dic[cli_name].append(current_port)
            else:
                graph.in_ports.append(current_port)
                graph.clients_in_dic[cli_name].append(current_port)

    return graph


def _run_command(command):
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    output, error = process.communicate()
    returncode = 0 if process.returncode == 0 else 1

    return output, error, returncode

